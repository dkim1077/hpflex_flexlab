**HP-FLEX Model Predictive Controller**

This is the repository of the model predictive controller (MPC) software for the HPFLEX project, funded by California Energy Commission (CEC). 
The MPC aims at providing load flexibility of heat pumps (HP) while maintaining zone air temperature within a band. 
More detailed descriptions for overall flow of the proposed approach, the mathematical formulation of the optimal control problem, and case studies will be described in the document (./doc/). 
It is a preliminary repository not the final MPC software. Once the MPC is validated using the FLEXLAB testbed, this repo will be transferred to an official LBNL repository. 

## Setup
This module was developed with Python 3.6. The MPC-library relies on the following external python libraries:

-[pyglik](https://pyglpk.readthedocs.io/en/latest/)

``` conda install -c conda-forge pyglpk ```

-[python-control package](https://python-control.readthedocs.io/en/0.9.1/intro.html#installation)

``` conda install -c conda-forge control slycot ```


## Example
Once the external python libraries are installed, one can run the following example with the python. 

-./MPC/GLPK_UMPC.py



## Library description
This MPC library is a free open-source. The library contains the followings.


## Authors
- Donghun Kim (donghunkim@lbl.gov)
- Sangwoo Ham (sham@lbl.gov)

## License

## Links
