#me={'a': np.mat(np.array([[0.9,0],[0,0.9]])), 'b': np.mat(np.array([[1],[1]])), 'c': np.mat(np.array([1,1]))}
# from H_sysa import H_sysa # first is file name the second is function name
# (Aa,Ba,Ca,Ka)=H_sysa(me)
import numpy as np
from control import dare
import scipy as sp
import copy

def H_sysa(me,*varargin,**varkeyargin):  #function sysa=H_sysa(me,varargin)
    if len(varargin) is not 0:
        opt=varargin[0]
    else:
        opt='OE'
    if len(varkeyargin) is not 0:
        rho=varkeyargin['rho']
    else:
        rho=1.
# if ~isempty(varargin)% appending output integrator
#   opt=varargin{1};
#else
#    opt='OE';
#end
    try: 
        me['a']
    except:
        me_old=copy.copy(me);
        me=dict();
        me['a']=me_old.A;
        me['b']=me_old.B;
        me['c']=me_old.C;
        
    n=me['a'].shape[0]
    m=me['b'].shape[1]
    p=me['c'].shape[0]
    if opt is 'OE':
        A=np.mat(sp.linalg.block_diag(me['a'],rho*np.mat(np.eye(p))))
        B=np.vstack((me['b'],np.mat(np.zeros((p,m)))))
        C=np.hstack((me['c'],np.mat(np.eye(p))))
        Q=sp.linalg.block_diag(5*1e-1*np.mat(np.eye(n)),np.mat(np.eye(p)));
        na=n+p;
    elif opt is 'IE':
        print('not avaiable')
        #A=[me.a, me.b*eye(m,m); 
         # zeros(m,n), eye(m,m)];
        #B=[me.b;zeros(m,m)];
        #C=[me.c,zeros(p,m)];
        #D=zeros(p,m);
        #na=n+m;
        #Q=1*eye(na);
    elif opt is 'KF':
        #print('not avaiable')
        A=me.a;
        B=me.b;
        C=me.c;
        na=n;
        

    R=1*1e-1*np.mat(np.eye(p))  # stick to measurement since measurment is more reliable
    #P=dare(A',C',blkdiag(me.*me.Noisevar*me.k',1e-1*eye(p)),me.Noisevar);
    Pdum=dare(A.T,C.T,Q,R) 
    P=np.mat(Pdum[0])
    try:
        K=A*P*C.T/(R+C*P*C.T)
    except:
        K=A*P*C.T*np.linalg.inv(R+C*P*C.T)
            
    Aa=A
    Ba=B
    Ca=C
    Ka=K 
    return Aa, Ba, Ca, Ka
