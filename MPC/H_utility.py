from __future__ import print_function
import numpy as np
from matplotlib.pyplot import *
import control.matlab as ctrl
import scipy as sp
import copy
import pandas as pd
import datetime
import scipy.signal
import matplotlib.dates
#from __future__ import print_function
import os

def H_reindex(df,tnew,**varargindic):
    """
    df=DataFrame(data=np.random.random((10,2)),columns=['a','b'])
    df.index=date_range('2020','2020-01-10',freq='1D')
    tnew=date_range('2019-12-31','2020-01-10',freq='1H')
    """
    
    assert isinstance(df,pd.core.frame.DataFrame), 'input should be dataframe obj'
    
    # convert timestamp to number, and interpolate
    df=df.fillna(method='bfill')
    df=df.fillna(method='ffill')
    
    if isinstance(df.index[0],pd.core.indexes.range.RangeIndex):
        assert isinstance(tnew, 'np.ndarray')
        tdatanum=df['time'].to_numpy() # unit sec
        tnewnum =tnew # unit sec
    else:
        assert isinstance(df.index, pd.core.indexes.datetimes.DatetimeIndex) and (df.index[0].tz== tnew[0].tz)
        tdatanum=np.array([k.timestamp() for k in df.index])
        tnewnum =np.array([k.timestamp() for k in tnew])
        
    data=df.to_numpy()
    datanew=np.vstack([np.interp(tnewnum,tdatanum,data[:,k]) for k in range(data.shape[1])]).T
    dfnew=pd.DataFrame(data=datanew,columns=df.columns,index=tnew)
    
    if len(varargindic)!=0:
        if varargindic['wannaplot']:
            figure()
            ax= subplot(211)
            df.plot(style='o-',ax=ax)
            ax=subplot(212)
            dfnew.plot(style='o-',ax=ax)
            legend(['original','interp'])
    return dfnew
    
    
def ts2nnARMAIO(dfy0,na, nb):
    # AutoRegression
    # nb=number of measurement
    # inputs: 1 dimensional dataframe
    # outputs: nb dimentionsal dataframe for current & past data (input profile)
    #          na dimentionsal dataframe for future data (output profile)
    # assert dfy.shape[1] == 1
    
    
    if isinstance(dfy0,np.ndarray):
        dfy=pd.DataFrame(data=dfy0,columns=['y'])
    else:
        dfy=dfy0
         
            
    df=dfy.copy()
    for j in range(nb):
        df[df.columns[0]+'[k-'+str(j)+']']=df[df.columns[0]].shift(j)
    # na=number of prediction
    for j in range(1,na+1):
        df[df.columns[0]+'[k+'+str(j)+']']=df[df.columns[0]].shift(-j)
    df=df.dropna()
    I=df.iloc[:,1:(nb+1)]
    O=df.iloc[:,(nb+1):]
    
    if isinstance(dfy0,np.ndarray):
        I=I.to_numpy()
        O=O.to_numpy()
    return I, O

def ts2nnARMAXIO(df0,na,nb):

    # nb=number of measurement
    # inputs: 2 (!) dimensional dataframe 
    #         the name of outputname
    # e.g. inputs = k    y     u 
    #               0    y[0]  u[0]
    #               1    y[1]  u[1]
    #               2    y[2]  u[2]
    # outputs: nb dimentionsal dataframe for current & past data (input profile)
    #          na dimentionsal dataframe for future data (output profile)
    # e.g. outputs
    # O= y[k+1],y[k+2],y[k+3],...
    # I = y[k-1],y[k-2],y[k-3],...,y[k-nb],u[k+1],u[k+2],u[k+3],....
    
    if isinstance(df0,np.ndarray):
        dfyu=pd.DataFrame(data=df0,columns=['y','u'])
        outputname='y'
    else:
        dfyu=df0
        outputname=df0.columns[0]
        
    df=dfyu.copy()
    I=DataFrame()
    O=DataFrame()
    for k in list(df.columns):
        dummyI, dummyO =ts2nnARMAIO(df[k].to_frame(),na, nb)
        if outputname is k:
            I=concat([I,dummyI],axis=1)
            O=concat([O,dummyO])    
        else:
            I=concat([I,dummyO],axis=1)
    if isinstance(df0,np.ndarray):
       I=I.to_numpy()
       O=O.to_numpy()
       
    return I, O
    
def H_nnforecast(df0,na,nb,**varargindic): # dataframe must be [output, input] order
    if df0.shape[1]==1:# only one timeseries
        wannaARMAX=False
        if isinstance(df0,np.ndarray):
            df=pd.DataFrame(data=df0,columns=['y'])
        else:
            df=df0.copy()                
    elif df0.shape[1]==2: # two timeseries
        wannaARMAX=True
        if isinstance(df0,np.ndarray):
            df=pd.DataFrame(data=df0,columns=['y','u'])
        else:
            df=df0.copy()                
    else:
        error()
    
    N=df0.shape[0]
    model=tfk.models.Sequential()        
    nnhl=30 # number of output for hiddenlayer
    epochs=500
    
    traindata=df.iloc[:int(0.7*N)]
    validata=df.iloc[int(0.7*N):]

    if not wannaARMAX:
        I,O  =ts2nnARMAIO(traindata,na,nb)
        Iv,Ov=ts2nnARMAIO(validata ,na,nb)
        model.add(tfk.layers.Dense(nnhl,activation=tfk.activations.tanh,input_dim=nb))
        model.add(tfk.layers.Dense(na))
    else:
        I,O  =ts2nnARMAXIO(traindata,na,nb)
        Iv,Ov=ts2nnARMAXIO(validata ,na,nb)
        model.add(tfk.layers.Dense(nnhl,activation=tfk.activations.tanh,input_dim=nb+na))
        model.add(tfk.layers.Dense(na))
    
    
    model.summary()
    model.compile(loss=tfk.losses.MeanSquaredError(),
                  optimizer=tfk.optimizers.Adam(),
                  metrics=[tfk.metrics.MeanAbsoluteError()])

    model.fit(I.to_numpy(),O.to_numpy(), epochs=epochs,verbose=0)
    
    figure(figsize=(15,5))
    for k in fix(linspace(0,Iv.shape[0],num=20)).astype(int):#range(I.shape[0]):
        try:
            ypred= model.predict(reshape(Iv.to_numpy()[k,:],[1,-1]))
            plot(arange(0,Ov.shape[0],1),Ov.to_numpy()[:,0],'r--')
            plot(arange(k,k+na,1),squeeze(ypred),linewidth=3)
        except:
            pass
    
    return model
    
    # df.plot(style='o-')
    # dfnew.plot(style='o-')
    
    return dfnew
    
def date2num(datetype,**varargdic):
    """
    dates='2012/02/01 00:00:30'
    tsn=date2num(dates,tz='US/Pacific')
    ts=num2date(tsn,tz='US/Pacific')
    print(ts)
    """
    if len(varargdic)==0:
        tz='US/Pacific'
    else:
        tz=varargdic['tz']
    if isinstance(datetype,pandas._libs.tslibs.timestamps.Timestamp):
        ts=datetype
    else:
        ts=pd.Timestamp(datetype,tz=tz) # timestamp obj
    tsn=ts.timestamp()
    return tsn
    
def num2date(tsn,**varargdic):
    
    if len(varargdic)==0:
        tz='US/Pacific'
    else:
        tz=varargdic['tz']
    ts=pd.Timestamp(tsn,unit='s',tz=tz) # timestamp obj
    return ts
    
def H_filtfilt(N,x,**varargindic):
    # scipy.signal.filtfilt(np.ones((5)),1./5*np.ones(1),random.random(100))
    x0=x[0]
    y=scipy.signal.filtfilt(np.ones(N),N*np.ones(1),H_iscolumn(x).T-x0)+x0
    y=y.reshape(x.shape)
    if len(varargindic)!=0:
        if varargindic['wannaplot']:
            figure()
            plot(y)
            plot(x,'r--')
            legend(['filtered','original'])
    return(y)

def filtfilt(N,x):
    # scipy.signal.filtfilt(np.ones((5)),1./5*np.ones(1),random.random(100))
    x0=x[0]
    y=scipy.signal.filtfilt(np.ones(N),N*np.ones(1),H_iscolumn(x).T-x0)+x0
    y=y.reshape(x.shape)
    return(y)
    
    
def H_date2num(dates):
    """ python datetime |---> python datenumber (note: timezone info will disappear.)
    dates=pd.date_range('2018-08-01 00:00:00','2018-08-01 00:15:00',freq='15T')
    pythondatenum=H_date2num(dates)    
    """
    try:
        phthondatenum=dates.timestamp()
        # or 
        # phthondatenum=matplotlib.dates(dates)
    except:
        # phthondatenum=np.zeros((len(dates),1))
        # for k in range(len(dates)):
        #     phthondatenum[k]=pd.Timestamp(dates[k]).timestamp()   
        phthondatenum=np.hstack([k.timestamp() for k in dates])
    #phthondatenum=to_datetime(phthondatenum)
    #matplotlib.dates.date2num(Timestamp('2010-01-01'))
    return phthondatenum
    
def H_num2date(phthondatenum,tz=None): 
    """python datesnumber, timezone |---> python datetime 
    dates=pd.date_range('2018-08-01 00:00:00','2018-08-01 00:15:00',freq='15T',tz='US/Pacific')
    phthondatenum=H_date2num(dates)
    H_num2date(phthondatenum,tz='US/Pacific')
    """
    try:
        phthondatenum=phthondatenum.squeeze()
    except:
        phthondatenum=phthondatenum
    try:
        dates=pd.Timestamp(phthondatenum,unit='s',tz=tz)
    except:
        dates=[pd.Timestamp(numk,unit='s',tz=tz) for numk in phthondatenum]            
    return dates
    
def H_date2simtime(dates,ref,tz=None): 
    """ python datetime, timezone |---> H_defined datenumber
    tz='US/Pacific'; None
    dates=pd.date_range('2018-08-01 00:00:00','2018-08-01 00:15:00',freq='15T',tz=tz)
    simtime=H_date2simtime(dates,'2018',tz=tz)
    print(H_simtime2date(simtime,'2018',tz=tz)    )
    """
    pythondateref=pd.Timestamp(ref,tz=tz)
    simtime=H_date2num(dates)-pythondateref.timestamp()
    return simtime

def H_simtime2date(simtime,ref,tz=None):
    assert isinstance(ref,str)
    phthondatenumref=pd.Timestamp(ref,tz=tz).timestamp()
    phthondatenum=simtime+phthondatenumref
    dates=H_num2date(phthondatenum,tz=tz)
    return dates
    
def H_random(N,xl,xu):
    x=xl+np.random.random(N)*(xu-xl)
    x=np.array([x]).T
    return x  


def H_filter(b,a,x):
    (n,m)=x.shape    
    if n<m:
        error()
    ICoffset=np.kron(x[0,:],np.ones((n,1)))
    X=x-ICoffset
    dum=list()
    b=np.squeeze(b)
    for ic in range(m):
        try:
            dum.append(sp.signal.filtfilt(b,a,X[:,ic].T))
        except:
            dum.append(sp.signal.filtfilt(b,a,X[:,ic]))   
    Xf=np.mat(np.vstack(dum).T)
    xf=Xf+ICoffset;
    return xf

def H_offset(x): # offset with column average like matlab
    (n,m)=x.shape
    xs=x-np.kron(np.ones((n,1)),np.mean(x,axis=0))
    return xs

def H_iscolumn(x):
    
    if x.ndim is 1:
        x=x.reshape((len(x),1)) # single to two 2
        #error('singlesize')
    nm=x.shape   
    n=nm[0]
    m=nm[1]

    if n<m:
        xc=x.T
    else:
        xc=x;
    return xc
    
def H_shift(x,opt):
    import copy
    xnew=0*copy.copy(x)
    if opt is 'd':
        xnew[:-1]=x[1:]
    elif opt is 'u':
        xnew[1:]=x[:-1]
    else:
        error('')
        
    return xnew
        

def H_c2dz(A,B,C,D,T): # just zero-order holder
    n=A.shape[0];
    invA=np.mat(np.linalg.inv(A));
    expAT=sp.linalg.expm(A*T);
    Aint=invA*np.mat(expAT-np.eye(n));
    Ad=expAT;
    Bd=Aint*B;
    Cd=C;
    Dd=D;
    return (Ad,Bd,Cd,Dd)

def H_c2dm(A,B,C,D,T): # given Continous time (A,B,C,D)
    # if I change setpoint at [k] for a perfectly controlled system at x[k]
    # 1. I am interested in energy [k,k+1), not power at [k+1]
    # this means my output is int(y_continous)
    # 2. since state would change during [k,k+1), I need somewhat integral form for C & D 

    n=A.shape[0];
    invA=np.mat(np.linalg.inv(A));
    expAT=sp.linalg.expm(A*T);
    Aint=invA*np.mat(expAT-np.eye(n));
    Ad=expAT;
    Bd=Aint*B;
    Cm=1./T*C*Aint;
    Dm=C*invA*(1/T*Aint-np.eye(n))*B+D;
    return (Ad,Bd,Cm,Dm)
    
def H_controller(y,r,Ierr,ctrltype,*varargin):
    err=np.mat(y)-np.mat(r);
    Ierr=err+np.mat(Ierr);
    if ctrltype is 'PI':
        Kp=varargin[0];
        Ki=varargin[1];
        u=Kp*err+Ki*Ierr;
    elif ctrltype is 'ONOFF':
        if err>0.5:
            u=-np.mat(5);
        elif err<-0.5:
            u=np.mat(0);
        else:
            u=varargin[1];
    return u,Ierr   

def H_RC(Ts_hr,**varargin_key):
    Ts_min=Ts_hr*60
#    from control.matlab import *
#    from Grey_Structure1z import *
    from Grey_Structure1z import Grey_Structure1z
    import copy
    cc=np.concatenate
    mm=np.matmul
    descript=dict()
    
    if len(varargin_key)==0:
        theta=np.array([3.987,  1.183,       0.4788,    29.38,   2.845,    0]);
    else:
        theta=varargin_key['theta']
    #      Cw kWh/oC, Cz kWh/oC, Rzw oC/kW, Roz oC/kW, As m^2, p [-].
    (A, B, C, D)=Grey_Structure1z(theta,1)
    # Qheatingcap, Toa, solar, Qheatgain
    
    descript['theta']='Cw,Cz,Rzw,Roz,As,p'
    descript['x']='Tz,Tw'
    descript['u']=['Qheat(+)[kW]','Toa[C]','qsol[kW/m2]','Qg[kW]']
    G=ctrl.ss(A,B,C,D)    
    # creat discrete model from the continous model with sampling time of 5 min
    dsys=ctrl.c2d(G,Ts_min*1./60,method='zoh')
    
    dsys.dt=1.; # change time unit to step
    print(descript.keys())
    for k  in descript.keys():
        print(k+':'+str(descript[k]))
        
    return G,dsys,theta

def H_estimateRo(Tz,Toa,Qh,Ts_hr):
    N=Tz.shape[0]
    Windowsize=int(3*24*1./Ts_hr)
    Nblk=int(N*1./Windowsize)
    TZ=Tz[:Windowsize*Nblk]
    TOA=Toa[:Windowsize*Nblk]
    QH=Qh[:Windowsize*Nblk]
    I=np.eye(Nblk)
    avg=1./Windowsize*np.ones((1,Windowsize))
    Tavg=np.kron(avg,I)
    Tzavg=np.mat(Tavg)*np.mat(TZ)
    Toavg=np.mat(Tavg)*np.mat(TOA)
    Qhavg=np.mat(Tavg)*np.mat(QH)
    figure(1001)
    plot(Tzavg-Toavg,Qhavg,'o')
    xlabel(['Tz-To [^oC]'])
    ylabel(['Qh [kW]'])
    PHI=np.hstack((Tzavg-Toavg,np.ones((Nblk,1))))
    theta=np.linalg.inv(PHI.T*PHI)*PHI.T*Qhavg
    Roa=1./theta[0]
    Qh0=theta[1]
    return Roa, Qh0

def H_schedule(tsec,ON_period,ON_value,OFF_value):
    if not isinstance(tsec,pd.core.indexes.datetimes.DatetimeIndex):
        hr=tsec*1./3600%24
    else:
        timestampobj=tsec
        #tz=str(timestampobj.tz)
        hr=timestampobj.hour.values
        
    schedule=ON_value*((ON_period[0]<=hr) & (hr<=ON_period[1]))+OFF_value*((hr<ON_period[0]) | (hr>ON_period[1]))
    schedule=H_iscolumn(schedule)    
    return schedule    
    #step(tsec*1./3600,y,where='post')

def H_plots(x,y,*varargin):
    from H_splot import H_splot
    Ys=H_splot(x,y,varargin)
    return Ys
    
def H_plotc(y1,y2):
    try:
        miny=min(min(y1),min(y2))
        maxy=max(max(y1),max(y2))
    except:
        miny=min(min(y1),min(y2))[0,0]
        maxy=max(max(y1),max(y2))[0,0]
        
    
    
    t=np.linspace(min(miny,0),maxy,20)
    
    plot(y1,y2,'o', t,t,'k-', t,t*1.1,'--', t,t*0.9,'--')  
    
    xlim([min(miny,0),maxy])
    ylim([min(miny,0),maxy])
    xlabel('y1')
    ylabel('y2')
    grid(True)
    
    

def H_bound(x,l,u):
    n=x.size
    xcut=x
    if type(l) is float or type(u) is int:
        for i in range(n):
            xcut[i]=min(max(x[i],l),u)
    else:
        for i in range(n):
            xcut[i]=min(max(x[i],l[i]),u[i])

    return xcut
    
#%%==============================================================================
# Customized data manipulation for DataFrame obj    
#==============================================================================

def H_rplot(df,month,**varargindic): # representative plot
    # df=DataFrame(data=random.random((36*24,2)),columns=['a','b'],index=date_range('2020',periods=36*24, freq='1H'))
    # H_rplot(df,month,**varargindic)
    #assert isinstance(df,pandas.core.frame.DataFrame)
    #assert isinstance(df.index,pandas.core.indexes.datetimes.DatetimeIndex)
    assert month in df.index.month
    
    m=len(df.columns)
    df=df[df.index.month==month] # select e.g. Aug data
    df['hr']=df.index.hour # create hr column for subgrouping
    grouped=df.groupby('hr')
    hrlymean=list()
    hrlymax=list()
    hrlymin=list()
        
    for ihr in range(0,24):
        hrlymean.append(grouped.get_group(ihr).mean().values)
        hrlymax.append(grouped.get_group(ihr).max().values)
        hrlymin.append(grouped.get_group(ihr).min().values)
    dmean=np.vstack(hrlymean)
    dmax=np.vstack(hrlymax)
    dmin=np.vstack(hrlymin)
        
    figure(figsize=(15,5))
    plot(dmean[:,-1],dmean[:,0:-1],'o-');
    if len(varargindic)!=0:
        plot(dmean[:,-1],dmax[:,0:-1],'--');
        plot(dmean[:,-1],dmin[:,0:-1],'--');
                
    xlim([0,24]);xlabel('hr');
    #legend(arange(1,m+1).tolist())
    print(df.columns)
    legend(df.columns)
    grid(True)
    return dmean, dmax, dmin

def H_fitlab(x,f):
    xmin=np.min(x)
    xmax=np.max(x)
    x=xmin+(xmax-xmin)*np.random.random((100,1))
    y=f(x)
    plot(x,y,'o')
    grid(True)    
    
def H_fitlab2(xmin,xmax,f):
    n=np.size(xmin)
    N=1000
    x=np.mat(np.zeros((N,n)))

    for i in range(n):
        x[:,i]=xmin[i]+(xmax[i]-xmin[i])*np.mat(np.random.random((N,1)))
    y=f(x)
    names=list('y')
    for i in range(n):
        names.append('x'+str(i))
    df=pd.DataFrame(np.hstack((y,x)),columns=names)
    pd.plotting.scatter_matrix(df)
#%%==============================================================================
# unit conversion
#==============================================================================


def ton2kW(Qton):
    QkW=3.5168525*Qton
    return QkW
    
def kW2ton(QkW):
    Qton=.284345136*QkW
    return Qton

def c2f(Tc):
    Tf=9./5*Tc+32
    return Tf
def f2c(Tf):
    Tc=(Tf-32)*5./9
    return Tc
    
def k2c(Tk):
    Tc=Tk-273.15
    return Tc

def c2k(Tc):
    Tk=Tc+273.15
    return Tk

def kJ2kWh(PkJ):
    # 1kWh def=1kW*3600 sec
    PkWh=PkJ*1./3600
    return PkWh

def kWh2kJ(PkWh):
    PkJ=3600*PkWh
    return PkJ    
def ton2kJphr(Qton):
    QkJphr=12660.67023144*Qton;
    return QkJphr
    
def kJphr2ton(QkJphr):
    Qton=QkJphr*1./12660.67023144;
    return Qton
    
def kJ2tonhr(QkJ):
    # 1kJ= 1kW*1 sec = 1/3.5 ton*1./3600hr ==> 1kJ= 1/3.5*1/3600 [ton-hr]
    Qtonhr=kW2ton(QkJ)*1./3600;
    return Qtonhr
    
def tonhr2kJ(Qtonhr):
    QkJ=12660.67023144*Qtonhr;
    return QkJ
    
def cfm2kgs(cfm):
    m3s=0.00047194745*cfm;
    air_density=1.2;
    kgs=air_density*m3s;
    return kgs;
    
def kgs2cfm(kgs):
    air_density=1.2;    
    m3s=kgs/air_density;
    cfm=m3s/0.00047194745;
    return cfm;
    
    
def kgs2gpm(mkgs):
    rhow=1000;
    Vgpm=15850.32314*mkgs/rhow        
    return Vgpm

def gpm2kgs(Vgpm):    
    rhow=1000;
    mkgs=rhow*1./15850.32314*Vgpm
    return mkgs

def lbm2kg(mlb):
    mkg=0.453592*mlb
    return mkg
    
def kg2lbm(mkg):
    mlbm=mkg*1./0.453592
    return mlbm
    
def lbmhr2kgs(mlbmhr):
    mkgs= lbm2kg(mlbmhr)/3600
    return mkgs
    
def kgs2lbmhr(mkgs):
    mlbmhr=kg2lbm(mkgs)*3600
    return mlbmhr        
    
def BTUphrft2F2Wpm2K(h_BTUpft2F):
    h_Wpm2C=h_BTUpft2F*5.678263
    return  h_Wpm2C

def Wpm2K2BTUphrft2F(h_Wpm2C):
    h_BTUpft2F=h_Wpm2C*1./5.678263
    return h_BTUpft2F

def ft2m(Lft):
    Lm=Lft*0.3048
    return  Lm
    
def m2ft(Lm):
    Lft=Lm*1./0.3048
    return Lft
    
    
def ft22m2(Aft2):
    Am2=0.092903*Aft2
    return Am2

def BTUphr2W(Q_BTUphr):
    Q_W=0.29307107*Q_BTUphr
    return Q_W    

def W2BTUphr(Q_W):
    Q_BTUphr=Q_W*1./0.29307107
    return Q_BTUphr    

def myfunc(x):
    y=x**2 # x could be vector (then y is a vector)
    return y
    
def myfunc2(x):
    y=np.mat(np.zeros((x.shape[0],1)))
    y=np.multiply(x[:,0],x[:,0])+np.multiply(x[:,1],x[:,1])
    return y
    
def test(a,*varargin, **var_key_n_arg):
    
    print(type(varargin))
    print(type(var_key_n_arg))
    print(varargin is None)
    print(var_key_n_arg is None)
    try:
        for key, value in var_key_n_arg.iteritems(): # python 2.x
            print("%s == %s" %(key,value))
    except:
        for key, value in var_key_n_arg.items(): # python 3.x
            print("%s == %s" %(key,value))

# Made by Donghun Kim
# This function gives jacobian matrix (in L(R^m --> R^n)) of f: x |--> y , R^n --> R^m

def H_Jaco(f,x):
    #equal(type(myfunc),'function'), isinstance?

    fx = f(x);
    if len(fx.shape)==0: # scalar
        m=1;
    else:
        m = np.max(fx.shape) ;
        
    n = np.max(x.shape) ;
    J=np.zeros((m,n)); # J in L(R^m --> R^n)
    h = 1e-5;
    
    for j in range(n): # for each input cacluate graident of f w.r.t. xi
        Perterb = np.zeros(x.shape);
        Perterb[j]=1;
        dfdxi=(f(x + h*Perterb) - fx)/h;
        for i in range(m):
            if m==1:
                J[i,j]=dfdxi;
            else:
                J[i,j]=dfdxi[i];

    return J

#%%
if __name__ is '__main__':
    from IPython import get_ipython
    get_ipython().magic('matplotlib inline')
    test(1,3,b=3)
    #y=H_iscolumn(np.linspace(0,10,11))
    H_RC(Ts_hr=1)
    #H_plotc(Y,hatY)
    figure(2)
    H_fitlab(np.array([1,2]),myfunc)
    
    figure(3)
    H_fitlab2(np.array([-1,-1]),np.array([1,1]),myfunc2)
    
    x=np.array([[1,2]]).T
    J=H_Jaco(myfunc,x)
    
    dd=pd.date_range('2020',periods=500,freq='1H')
    T=np.linspace(0,10,num=500)
    df=pd.DataFrame(data=np.vstack((np.sin(T),np.cos(T))).T,columns=['a','b'])
    df.index=dd
    H_plotly(df)
    
