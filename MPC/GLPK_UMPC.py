# funciontality : functional tests based on simple senarios
#==============================================================================
# Original /home/adun6414/Work/H_functlibpy/H_MPC/src/H_UC_HP.py
#==============================================================================

"""
x[k+1] = A x[k] + Bu u[k] +Bw w[k] + K e[k]
y[k]   = C x[k]
where 
u=[u_h,u_c].T, w=[Toa,qsol,Qg].T
x=[T,p].T

T[k+1] = [A 0]T[k]+[Bu]u[k]+ [Bw] w[k] +Kx e[k]
p[k+1] = [0 I]p[k]+[0]     + [ 0]      +Ko 
y[k+1] = [C I]x[k]


min ( sum_{k=0}^{Np-1} ER[k]uh[k] + ER[k]uc[k] ) +wd*d + wu*gu + wl* gl
s.t. Tl[k]-gl <=Tz[k] <= Tu[k]+gu for all k in {1, ..., Np}
     uh[k]+uc[k]< =  d
     0 <= uh[k] < = uhmax
     0 <= uc[k] < = uhmax
     0 <= d
"""

import os
import sys
from H_utility import *
import pandas as pd
from matplotlib.pyplot import *
import datetime 
import matplotlib.dates as mpdates
from scipy.io import *
import numpy as np
from H_sysa import H_sysa
import PnP_subfunc
from H_blkdiag import H_blkdiag
import glpk
from scipy.linalg import block_diag

class H_UC_HP:
    def __init__(obj,MOD,Ts_hr,Np,Nblk,**varargindic):
        
        obj.Np0=Np;
        obj.Np=obj.Np0;
        obj.Ts_hr=Ts_hr;
        obj.Ts=Ts_hr*3600;
        
        obj.pre_day=-1
        obj.iter=0
        obj.MOD=MOD # x= Ax +Bu +Ke, y=Cx
        
        
        for key in varargindic.keys():
            setattr(obj,key,varargindic[key])
            
        for xuwyz in obj.mpcvariables.keys():
            setattr(obj,xuwyz+'_name',obj.mpcvariables[xuwyz])
            setattr(obj,'n'+xuwyz,len(obj.mpcvariables[xuwyz]))
        
        
        # saving prediction result files
        cur_dir=os.getcwd()
        obj.res=dict()
        
        
        obj.res['path_file']=varargindic['respathfile']+'_PRED.csv'
        obj.res['columns']=['cur_t', 'predic_t']+obj.x_name+obj.u_name+obj.w_name+obj.y_name
        try: # print header
            pd.read_csv(obj.res['path_file'])
        except:         
            pd.DataFrame(columns=obj.res['columns']).to_csv(obj.res['path_file'],mode = 'w',header=True)
                       
        obj.PRED=pd.DataFrame()
        obj.iter=0
        
        # %% copied from MILP_UMPC ================================
        obj.p=MOD['C'].shape[0]
        obj.Pmat0=np.array([1./obj.COPh, 1./obj.COPc])
        obj.m=obj.Pmat0.size
        obj.n=MOD['A'].shape[0]
        obj.mw=MOD['B'].shape[1]-obj.m
        
        assert obj.nx==obj.n
        assert obj.nu==obj.m
        assert obj.nw==obj.mw+3 # ['ER', 'Tl', 'Tu']
        assert obj.ny==obj.p+2 # ['Ph','Pc']
        
        obj.a=np.mat(MOD['A'])
        obj.bc=np.mat(MOD['B'])[:,:obj.m]
        obj.bw=np.mat(MOD['B'])[:,obj.m:] # <<< modification from [:,-1] 
        obj.c=np.mat(MOD['C'])
        obj.k=np.mat(MOD['K'])
        
        if obj.mw!=3: # Toa [^oC], qGHor[kW/m^2],Qg[kW]
            error('This code will work for general size of measured disturbances, so no worries')
                
        """
        # Obs: x[0] |--> [y[1].T, y[2].T,..].T
        # Mcs: [u[0].T,u[1].T,...,u[Np].T].T (in R^(m*Np)) |--> [y[1].T, y[2].T,.., y[Np+1].T].T (in R^(p*Np))
        """
        obj.Obs=PnP_subfunc.H_obs(obj.a,obj.c,obj.Np) 
        obj.Mcs=PnP_subfunc.H_mar(obj.a,obj.bc,obj.c,obj.Np) 
        obj.Mws=PnP_subfunc.H_mar(obj.a,obj.bw,obj.c,obj.Np)
        assert obj.Obs.shape==(obj.Np*obj.p,obj.n)
        assert obj.Mcs.shape==(obj.Np*obj.p,obj.Np*obj.m)
        assert obj.Mws.shape==(obj.Np*obj.p,obj.Np*obj.mw)
        
        # blocking +++++++++++++++++++
        obj.Nblk=Nblk
        if obj.Np%obj.Nblk !=0:
            error()
        obj.Tr={} # define empty dictionary
        obj.Tr['bu2pu']=H_blkdiag(np.kron(np.ones((int(obj.Np/obj.Nblk),1)),np.eye(obj.m)),obj.Nblk)
        obj.Tr['bw2pw']=H_blkdiag(np.kron(np.ones((int(obj.Np/obj.Nblk),1)),np.eye(obj.mw)),obj.Nblk)
        obj.Tr['by2py']=H_blkdiag(np.kron(np.ones((int(obj.Np/obj.Nblk),1)),np.eye(obj.p)),obj.Nblk)
        
        # Transformation from [u,v]^T--> [u,...,u,v,...,v]^T        
        obj.Tr['yavg']=H_blkdiag(np.kron(np.ones((1,int(obj.Np/obj.Nblk))),np.eye(obj.p))/(int(obj.Np/obj.Nblk)),obj.Nblk)# averaging y
        obj.Tr['wavg']=H_blkdiag(np.kron(np.ones((1,int(obj.Np/obj.Nblk))),np.eye(obj.mw))/(int(obj.Np/obj.Nblk)),obj.Nblk)# averaging w
        obj.Tr['uavg']=H_blkdiag(np.kron(np.ones((1,int(obj.Np/obj.Nblk))),np.eye(obj.m))/(int(obj.Np/obj.Nblk)),obj.Nblk)# averaging w for different rates for e.g. cooling & heating
        obj.Tr['savg']=H_blkdiag(np.kron(np.ones((1,int(obj.Np/obj.Nblk))),np.eye(1))/(int(obj.Np/obj.Nblk)),obj.Nblk)# averaging y
            
        
        # state responses not output responses
        obj.Obsx=PnP_subfunc.H_obs(obj.a,np.mat(np.eye(len(obj.a))),obj.Np)
        obj.Mcsx=PnP_subfunc.H_mar(obj.a,obj.bc,np.mat(np.eye(len(obj.a))),obj.Np)
        obj.Mwsx=PnP_subfunc.H_mar(obj.a,obj.bw,np.mat(np.eye(len(obj.a))),obj.Np)
        
        
        obj.Ob=obj.Tr['yavg']*obj.Obs;
        obj.Mc=obj.Tr['yavg']*obj.Mcs*obj.Tr['bu2pu'];
        obj.Mw=obj.Tr['yavg']*obj.Mws*obj.Tr['bw2pw'];
        
                
        obj.flag=0 # exit flag, 1: LIP -1:LP -2:Conv
        obj.w_on_d=0.001 # weight on demand Energy+weight*Demand
        obj.hatxk=np.mat(np.zeros((obj.n,1)))
        obj.inno=np.mat(np.zeros((obj.p,1)))
        
        # %% GLPK optimization problem setup ====================
        """
        define columnes of LP [u[0].T, u[1].T, ...., u[Np].T, d, gamma_l, gamma_u]
        define rows of LP 
        demand constratints: u1[1]+u2[1] <= d ===> Np
        temp upper bound constraints: y1[1] <= yub  +vu     ===> Np
        temp lower bound contrsinats: -y1[1] <= -ylb-vl   ===> Np
        """
        
        obj.NU=obj.Np*obj.m
        obj.Nd=1
        obj.Nyl=1
        obj.Nyu=1
        
        obj.Nie_d=obj.Np
        obj.Nie_yu=obj.Np*obj.p
        obj.Nie_yl=obj.Np*obj.p
        
       
        
    def controller(obj,x0,W,*varargin): #(matrix,matrix) |----> matrix (optiaml u-trajectory)        

        # solves optimal control problem
        WnB=W.copy();        
        assert WnB.shape[0] == obj.Np
        hatxk=H_iscolumn(x0)#obj.hatxk
		        
        # Time series to vector form
        YL=np.mat(WnB[:,obj.w_name.index('Tl')]).T
        YU=np.mat(WnB[:,obj.w_name.index('Tu')]).T
        Ws=np.mat(WnB[:,[obj.w_name.index('Toa'),obj.w_name.index('qsol'),obj.w_name.index('Qg')]]) # time series form
        W=np.reshape(Ws,(Ws.size,1))
        if W.shape[0]!=obj.Np*obj.mw:
            error()
        Rs=np.tile(np.mat(WnB[:,obj.w_name.index('ER')]).T,(1,2)) # times series
        R=np.reshape(Rs,(Rs.size,1))
        
        if R.shape[0]!=obj.Np*obj.m:
            error()

        # averaging future info for blocking
        YUblk=obj.Tr['yavg']*YU;
        YLblk=obj.Tr['yavg']*YL;
        Wblk=obj.Tr['wavg']*W;            
        Rblk=obj.Tr['uavg']*R;
        
        # construct LP coefficient matrix
        (F,Aie,bie)=obj.obj_bd(hatxk,YLblk,YUblk,Wblk,Rblk)
        lb = np.hstack((np.kron(np.ones(Np),np.array([0,0])),0,0,0))
        ub = np.hstack((np.kron(np.ones(Np),np.array([obj.uhmax,obj.ucmax])),np.max([obj.uhmax,obj.ucmax]),100,100))
        
        # %% glpk setup 
        lp=glpk.LPX()
        lp.obj.maximize=False
        lp.cols.add(obj.NU+obj.Nd+obj.Nyl+obj.Nyu)
        ci=0 # ci: column index
        for c in lp.cols:
            c.bounds= lb[ci], ub[ci]
            ci=ci+1
        
        lp.obj[:]=F.tolist()[0]
        
        lp.rows.add(obj.Nie_d+obj.Nie_yl+obj.Nie_yu)
        lp.matrix=np.array(np.reshape(Aie,(1,-1)))[0].tolist()
        
        ri=0 # ri: row index
        for r in lp.rows:
            r.bounds = None, bie[ri]
            ri=ri+1
            
        lp.simplex()               
        if lp.status == 'opt':
            obj.flag=1
            opt=np.array([c.primal for c in lp.cols])
            fval=lp.obj.value
            del(lp)
        else:
            obj.flag=0
            error('not converged!!!!')
            
        Uopt=np.mat(opt[0:obj.m*obj.Nblk]).T # last term is demand, Tu,Tl
        xsop=np.reshape(obj.Obsx*hatxk+obj.Mcsx*Uopt+obj.Mwsx*Wblk,(-1,obj.n))
        usop=np.reshape(Uopt,(-1,obj.m))
        return usop, fval, np.mat(xsop)

    def obj_bd(obj,hatxk,YL,YU,W,R):
        p1p2=np.kron(np.ones((1,obj.Nblk)),obj.Pmat0)
        f=np.mat(np.array(R.T)*np.array(p1p2)) # [R1(1)P1(1), R2(1)P2(1), R1(2)P1(2), R2(2)P2(2)...]
        ff=np.split(np.squeeze(np.array(f)),obj.Nblk) #(R1(1)P1(1), R2(1)P2(1)), (R1(2)P1(2), R2(2)P2(2)), ...
        #f=np.squeeze(np.kron(R.T,obj.Pmat0))
        # minimize R1(1)P1(1)*u1(1)+ R2(1)P2(1)*u2(1)+R1(2)P1(2)*u1(2)+R2(2)P2(2)u2(2)+ d + uviol + lviol
        F=np.concatenate((f,np.mat(np.hstack((obj.w_on_d,1e2,1e2)))),1) # with demand cost comfort cost
        #==============================================================================
        # [P'u(1)  -d]<=0
        # [P'u(2)  -d]<=0
        # PU <=d            ==>  P U  -d       <=0
        # Ox+MU<=Yu  +u     ==>  M U     -u     <= (YU -Ox-Mw*W)
        # Yl-l<=Ox+MU       ==> -M U        -l  <= -(YL-Ox-Mw*W)
        #==============================================================================
        #Aie=np.concatenate((np.kron(np.mat(np.diag(np.asarray(R.T)[0])),obj.Pmat0),obj.Mc,-obj.Mc))
        Aie=np.concatenate((PnP_subfunc.H_blk_diag(ff),obj.Mc,-obj.Mc))
        
        #Aie=np.concatenate((np.kron(np.eye(obj.Nblk),obj.Pmat0),obj.Mc,-obj.Mc))
        Aie=np.concatenate((Aie,block_diag(-np.ones((obj.Nblk,1)),-np.ones((obj.Nblk*obj.p,1)),-np.ones((obj.Nblk*obj.p,1)))),1)
        bie=np.concatenate((np.zeros((obj.Nblk,1)),YU-obj.Ob*hatxk-obj.Mw*W, -(YL-obj.Ob*hatxk-obj.Mw*W)))
        return (F,Aie,bie)#,ub)
                     

    def exeMPC(obj,cur_t,x0,Ws,**varargindic): # df2matrix + state estimation + controller + optimal predictions
        # Timestamp, dataframe, dataframe ---> dataframe
        
        if isinstance(Ws,pd.core.frame.DataFrame):
            assert x0.columns.to_list() == obj.x_name
            assert Ws.columns.to_list() == obj.w_name
            Ws=Ws.to_numpy()
            xk=x0.to_numpy()
        else:
            assert x0.size == len(obj.x_name)
            assert Ws.shape[1] == len(obj.w_name)
            xk=x0
            
        assert Ws.shape[0] == obj.Np0, print('Ws is in ', str(Ws.shape), 'obj.Np0: ', str(obj.Np0))
        obj.iter=obj.iter+1
        if 'adjustNp' in varargindic.keys():
            if varargindic['adjustNp']:
                error() # adjustNp was not coded yet.
                #Wf=obj.adjustNp(cur_t,x0,Ws)
        else:
            Wf=Ws
                
        if not ('overridempc' in list(varargindic.keys())):
            (Uop,Jop,Xop)= obj.controller(xk,Wf)#,wannaplot)  # Uop, Wf \in R^{Np \times nu or nw}
        else:
            Uop=varargindic['overridempc']
            assert Uop.shape[0]==obj.Np0
            Uop=Uop[:obj.Np,:] # due to adjustment of Np
           
        assert not np.any(np.isnan(Uop)), "NaN deteceted!!!!!!!!!!!!!!!!"
        y=np.hstack((np.vstack((xk[0,0],Xop[:-1,0])), 1./obj.COPh*Uop[:,0], 1./obj.COPc*Uop[:,1]))
        #(x,phi,y)=obj.modelprediction(xk,Wf,Uop)#,wannaplot)  # x\in R^{Np+1 \times nx}, phi= \in R^{Np \times 1}, y= \in R^{Np \times ny}
        predic_t=pd.date_range(start=cur_t,periods=obj.Np,freq=str(obj.Ts_hr)+'H')
        
        dft=pd.DataFrame(data={'cur_t':cur_t,'predic_t':predic_t});
        dfx=pd.DataFrame(data=np.vstack((xk,Xop[:-1,:])),columns=obj.x_name);
        dfu=pd.DataFrame(data=Uop,columns=obj.u_name);
        dfw=pd.DataFrame(data=Wf,columns=obj.w_name);
        dfy=pd.DataFrame(data=y,columns=obj.y_name);
        df=pd.concat([dft,dfx,dfu,dfw,dfy],axis=1)
        assert df.shape[0] == obj.Np
        assert all(df.columns == obj.res['columns'])
        
        # datasave===============================
        # obj.PRED=obj.PRED.append(df)
        df.to_csv(obj.res['path_file'],mode='a',header=False)
        
        
        obj.pre_day=obj.cur_day(cur_t); # save the current day as a previous one


        if 'wannaplot' in varargindic.keys():
            if varargindic['wannaplot']:
                obj.analysis(predic_analysis=False)
        
        Uops=pd.DataFrame(data=Uop,columns=obj.u_name,index=predic_t) 
        
        if 'preddata' in varargindic.keys():
            output = (Uops,df,Xop)
        else:
            output = Uops
        return output
        
    def adjustNp(obj,cur_t,x0,Ws,**varargindic): 
        # Ws is in array
        curday=obj.cur_day(cur_t)
        if obj.pre_day is not curday: # for the first sampling time at each day, reset Np as Np0
            obj.Np=obj.Np0
        else: # reduce Np and the corresponding W
            obj.Np=obj.Np-1
        print('Np adjusted:', obj.Np)
        Wf=Ws[:obj.Np,:] #Wf=Ws.iloc[:obj.Np,:].values   
        print('in adjustNp', 'adjusted W:', Wf.shape, 'orginal W', Ws.shape)
        
        return Wf
            
    def cur_day(obj,cur_t):
        if isinstance(cur_t,pd._libs.tslibs.timestamps.Timestamp):
            curday=cur_t.day;
        else:
            curday=np.floor((cur_t%(3600*24*365))/(3600*24))
        return curday
            
        
    def analysis(obj,**varargindic):
        
        pre=pd.read_csv(obj.res['path_file'])
        dummy=pre
        dummy['ER']=pre['ER'].mask(pre['ER']>1.5,10).to_frame()
        dummy['ER']=dummy['ER'].where(dummy['ER']>1.5,0)
        
        t0=pd.Timestamp(dummy.cur_t[0],tz='US/Pacific')
        
        groupdf=dummy.groupby('cur_t')
        timekeys=list(groupdf.indices.keys())
        plotperiod=1 
        figure(figsize=(10,8)) 
        
        for timekey in timekeys:

            df=groupdf.get_group(timekey)
            #dumt=squeeze(hstack([Timestamp(k).timestamp() for k in df['predic_t']]))
            #t=(dumt-dumt0)/3600
            timedeltas=[pd.Timestamp(k,tz='US/Pacific')-t0 for k in df['predic_t']]
            t=t0.hour+np.array([k.delta/1E9/3600 for k in timedeltas]) # unit: hour
            
        
            ax1=subplot(211)
            #plot(t,np.squeeze(df[['Tz','Tw']].apply(lambda x:x+22).to_numpy()))
            #plot(t,np.squeeze(df[['Tl','Tu']].apply(lambda x:x+22).to_numpy()))
            plot(t,np.squeeze(df[['Tz','Tw']].to_numpy()))
            plot(t,np.squeeze(df[['Tl','Tu']].to_numpy()))
            ax1.fill_between(t,0,50*df[['ER']].to_numpy().squeeze(),alpha=0.3,color='grey',step='post')
            ticks,labels=xticks()
            xticks(ticks,[int(k%24) for k in ticks])
            grid(True)
            legend(['Tz','Tw']);#,'Toa']);
            ylim([15,30])
            ylabel('Temperature $[^oC]$')
            
            # ax2=subplot(312)
            # plot(t,np.squeeze(df[['u_h','u_c']].to_numpy()))
            # ax2.fill_between(t,0,dummy.to_numpy(),alpha=0.3,color='grey',step='post')
            # grid(True)
            # ylabel('Heating capacity [kW]')
            # legend(['u_h','u_c'])
            
            
            ax3=subplot(212)
            plot(t,np.squeeze(df[['Pc','Ph']].to_numpy()),drawstyle="steps-post")
            ax3.fill_between(t,0,50*df[['ER']].to_numpy().squeeze(),alpha=0.3,color='grey',step='post')
            ticks,labels=xticks()
            xticks(ticks,[int(k%24) for k in ticks])
            grid(True)
            xlabel('hr')
            legend(['Pc','Ph'])
            ylabel('Power [kW]')
            ylim(0,3);
            # show()
            title(pd.Timestamp(timekey).round('T').strftime('%m/%d %H:%M'))
        #savefig(obj.res['path_file']+str(obj.iter)+'.png')
        show()
        #savemat(obj.res['path_file']+str(obj.iter)+'.mat',obj.__dict__)


class Emptyobject():
    pass

# %% conversion from u[0] (kW) to setpoint for k=1

#            for iRTU in range(1,obj.p+1):
#                if UOPT[iRTU-1,0]<0.1*obj.ub0[0,iRTU-1]: # very small energy then
#                print('===== SP compensated!!! ==============')
#                       SP[iRTU-1,0]=YUblk[iRTU-1,0]
##                if UOPT[iRTU-1,0]>0.95*obj.ub0[0,iRTU-1]:
#                        #SP(iRTU)=yk(iRTU)-3;
#            SP[UOPT<0.2]=YU[0,0]

#%% test object
if __name__ is '__main__':
    import scipy.signal
    import control as ctrl
    
    close('all')
    Ts_hr= 1; # unit: hour
    Np=2*24; # size prediction horizon
    respathfile='./res/dummy'
    sy=Emptyobject()
    sy.tz='US/Pacific'

    # spec
    
    theta=np.array([ 3.987 ,  1.183 ,  0.4788, 29.38*0.1  ,  2.845 ,  0.    ])
    (Gc,dsys,_)=H_RC(Ts_hr,theta=theta)
    (T,yout)=ctrl.step_response(dsys)
    inputchannel=['Qheat(+)[kW]','Toa[C]','qsol[kW/m2]','Qg[kW]']
    figure()
    for i in range(dsys.B.shape[1]):
        subplot(dsys.B.shape[1],1,i+1)
        plot(T,yout[0][i,:])
        legend(inputchannel[i])
        xlabel('timestep[hr]: '+str(Ts_hr))
    
    sy.respathfile=respathfile; #externally defined
    # %% mpc parameters ============================
    
    sy.Ts_hr= Ts_hr; 
    sy.Np = Np;
    
    # model structure change
    MPCMOD=ctrl.StateSpace(dsys.A,np.hstack((dsys.B[:,0],-1*dsys.B[:,0],dsys.B[:,1:])), dsys.C,np.zeros((1,5)),1)
    Aa, Ba, Ca, Ka= H_sysa(MPCMOD)
    
    MOD={'A':Aa, 'B':Ba, 'C':Ca, 'K':Ka}
    
    sy.MOD=MOD
    sy.mpcvariables={'x':['Tz','Tw', 'po'],
                  'u':['u_h','u_c'], 
                  'w':['Toa', 'qsol','Qg','ER','Tl','Tu'],
                  'y':['Tz','Ph','Pc'],
                  'z':['P']}
    
    sy.COPh=5
    sy.COPc=3 
    sy.uhmax=ton2kW(3)
    sy.ucmax=ton2kW(2)
    sy.w_d=0*1e2
    sy.w_v=1e3
    
    obj=H_UC_HP(sy.MOD,sy.Ts_hr,sy.Np,sy.Np,mpcvariables=sy.mpcvariables,
                COPh=sy.COPh,COPc=sy.COPc,
                uhmax=sy.uhmax,ucmax=sy.ucmax,
                w_d=sy.w_d, w_v=sy.w_v,
                tz=sy.tz,respathfile=sy.respathfile)
    
    # %% simulation setup
    cur_ts=pd.Timestamp('2020-07',tz=sy.tz)
    T=pd.date_range(start=cur_ts,periods=3*sy.Np,freq=str(Ts_hr)+'H')
    
    # numT=np.array([tt.timestamp() for tt in T])
    # retriving test from number to date
    # T2=[Timestamp(tt, unit='s' , tz='America/Los_Angeles') for tt in numT]
    
    
    numT=H_date2num(T).squeeze()
    cur_t=numT[0]
    
    # %% measured or known disturbance signals
    Toa=7+10*np.cos(2*np.pi/(3600*24)*numT) # 7 C: Heating, 30: Cooling
    qsol=0*Toa
    Qg=0*Toa
    
    ER0=H_schedule(T,[12+4,12+9],2,1)
    ER=np.squeeze(np.multiply(np.matrix(np.linspace(1,1.1,np.size(ER0))).T,ER0))
    Tl=22+np.squeeze(H_schedule(T,[9,12+7],-1,-3))
    Th=22+np.squeeze(H_schedule(T,[9,12+7],1,3))
    W=np.vstack((Toa,qsol,Qg,ER,Tl,Th)).T
    
    # caseformat=1
    
    # if caseformat==1:
    # ## Input format : dataframe with index of timestamp obj
    #     Ws=pd.DataFrame(data=W,index=T,columns=sy.mpcvariables['w'])
    #     x0s=pd.DataFrame(data=np.matrix([22+0.2,22+0.2,0]),columns=sy.mpcvariables['x'],index=[cur_ts])
    #     (Uo,df,Xop)=obj.exeMPC(cur_ts,x0s,Ws.iloc[:obj.Np,:],wannaplot=False,preddata=True)
    # ##Ws.plot(drawstyle="steps-post",subplots=True,grid=True)
    # elif caseformat==2:
    # ## Input format : dataframe with index of a number
    #     Ws=pd.DataFrame(data=W,index=numT,columns=sy.mpcvariables['w'])
    #     x0s=pd.DataFrame(data=np.matrix([22+0.2,22+0.2,0]),columns=sy.mpcvariables['x'],index=[cur_t])
    #     (Uo,df,Xop)=obj.exeMPC(cur_t,x0s,Ws.iloc[:obj.Np,:],wannaplot=False,preddata=True)
    # elif caseformat==3:
    # ## Input format : matrix
    #     x0=np.matrix([22+0.2,22+0.2,0])
    #     (Uo,df,Xop)=obj.exeMPC(cur_t,x0,W[:obj.Np,:],wannaplot=False,preddata=True)
    
    # obj.analysis(predic_analysis=False)  
    
    #%% functional tests for adaptive scheme
    Ws=pd.DataFrame(data=W,index=T,columns=sy.mpcvariables['w'])
    x0s=pd.DataFrame(data=np.matrix([22.2,22.2,0]),columns=sy.mpcvariables['x'],index=[cur_ts])
    xk=x0s.copy()
    cur_tt=cur_ts
    for k in range(1,int(5)):
        cur_tt=cur_tt+pd.Timedelta(hours=obj.Ts_hr)
        # recieve measurments from volttron (zone air temp, mean heating runtime [-], mean cooling runtime[-])
        # To DO 1: need to include kalman filter + feed disturbance prediction later
        # ---> xk & Ws
        Uo=obj.exeMPC(cur_tt,xk,Ws.iloc[k:(k+Np),:],wannaplot=False)
        # To DO 2: needd to convert Uo to setpoint
        # To DO 3: feed optimal decision to true system
            
        x=H_iscolumn(xk.to_numpy())
        u=H_iscolumn(Uo.iloc[0,:].to_numpy())
        w=H_iscolumn(Ws.iloc[k,:].to_numpy())
        windex=[obj.w_name.index(k) for k in obj.w_name if k in ['Toa','qsol','Qg']]
        x=obj.a@x+obj.bc@u+obj.bw@w[windex] # update true system
        xk=pd.DataFrame(data=x.T,columns=obj.x_name,index=[cur_t])
     
    obj.analysis(predic_analysis=False)
