import scipy as sp
def H_blkdiag(A,n):
    AA=A
    for i in range(2,n+1):
        AA=sp.linalg.block_diag(AA,A)
    return sp.mat(AA)        