# Simple RC model strcture
# CzTz'=-(1/Rw+1/Ro)Tz+1/RoTo+(Qm+Qg)+(1-p)AsQs
# CwTw'=-1/Rw*Tw + 1/Rw*Tz+pAs(Qs)
# parameter: Cw,Cz,Rwz,Ro,p,A 6
# As: equivalnat window area
# p: franction of SHG that goes to wall
# Example
# theta=[3.987,  1.183,       0.4788,    29.38,   2.845,    0];
#    Cw kWh/oC, Cz kWh/oC, Rzw oC/kW, Roz oC/kW, As m^2, p [-].
#    Cw=theta0[0,0]
#    Cz=theta0[0,1]
#    Rzw=theta0[0,2]
#    Rzo=theta0[0,3]
#    As=theta0[0,4]
#    p=theta0[0,5]
#    rhoa=theta0[0,6]
#    rhob=theta0[0,7]
# E=diag([Cz,Cw]);
# A=[-(1/Roz+1/Rzw), 1/Rzw;
#     1/Rzw, -1/Rzw];
#   [Qm, Toa, sol, Qg
# B=[1, 1/Roz, As*(1-p), 1;
#    0, 0    , As*p  ,   0];
# C=[1,0];
# D=zeros(1,4);
# * Ts [hr]  sampling time [hour] e.g. 5 min sampling time ==> Ts=1/12 [hr]

import numpy as np

def Grey_Structure1z(theta0,NRTU,**varargindic):
    
    theta0=np.squeeze(np.array(theta0))
#    if theta0.shape[1] != 8: # row vector
#        theta0=theta0.T
    Cw=theta0[0]
    Cz=theta0[1]
    Rzw=theta0[2]
    Rzo=theta0[3]
    As=theta0[4]
    p=theta0[5]
#    rhoa=theta0[0,6]
#    rhob=theta0[0,7]
    
    
    Cs=np.vstack((Cz,Cw))
    invE=np.matrix(np.diag(np.array(1./Cs).T[0]))
    
    H=np.mat([[-(1/Rzo+1/Rzw), 1/Rzw], \
              [1/Rzw, -1/Rzw]]);
    
    
    A=invE*H;
    #[zeros(NRTU,NRTU);diag(Q)]==>
    #np.vstack((np.matrix(np.zeros((NRTU,NRTU))),np.matrix(np.diag(np.array(Q.T)[0]))))
    if len(varargindic) is 0:    
        B=invE* \
         np.mat([ [1, 1/Rzo, As*(1-p), 1],\
                  [0, 0    , As*p  ,   0]]) 
    elif varargindic['Qc'] is True:
        B=invE* \
         np.mat([ [-1, 1/Rzo, As*(1-p), 1],\
                  [0, 0    , As*p  ,   0]])
    else:
        error();              
    C=np.matrix([1, 0]);
    # discretize grey sys for later augmentation
    D=np.mat(np.zeros((NRTU,NRTU+3)) )
    
    return A, B, C, D
     
    
if __name__=='__main__':
    
    NRTU=1
    theta=[3.987,  1.183,       0.4788,    29.38,   2.845,    0];
#      Cw kWh/oC, Cz kWh/oC, Rzw oC/kW, Roz oC/kW, As m^2, p [-].
    
    Ag,Bg,Cg,Dg=Grey_Structure1z(thetao,1) 
    
